# sae init sys
Tout d'abord j'ai effectué l'installation sur un ordinateur MSI et j'ai décidé de faire un dual boot.    
Ainsi pour faire un dual boot avec xubuntu il faut d'abord se munir d'une clé usb avec suffisamment de stockage disponible, pour ma part j'ai pris une clé usb vierge qui nous a été donné au début de la SAE.

Il faut ensuite installé sur cette clé l'image ISO de xubuntu, que l'on trouve sur le site officiel. Il faut installé le logiciel Rufus pour pouvoir formater  et transformer la clé usb en clé bootable avec l'image téléchargée en amont. Pour pouvoir vous resservir de la clé il faudra la reformater.

On peut ensuite commencer le dual boot, pour cela il faut se rendre dans les paramètres windows et sélectionner "mise à jour et sécurité" puis dans "récupération" et cliquer sur "démarrage avancé".

Dans le menu "choisir une option" il faut sélectionner "utiliser un périphérique" puis ensuite choisir la clé bootable.
Suite à cela on sélectionne xubuntu puis après il faut suivre les instructions pour configurer xubuntu.

Une fois fais on doit redémarrer la machine et si la machine redémarre sur windows sans laisser le choix de redémarrer sur xubuntu, il faut rendre dans le BIOS pour changer les priorités de boot afin de faire en sorte que xubuntu soit devant windows sur la liste de priorité, pour que la machine démarre sur xubuntu en priorité.
Pour faire cela dans le BIOS il faut aller dans "Advanced BIOS features" puis "boot device priority" et changer l'ordre de la liste.

Enfin l'installation est finie, on peut télécharger les logiciels dont on a besoin et finir de configurer son environnement de travail.